<?php

declare(strict_types=1);

namespace Grifix\StateMachine\Exceptions;

final class InvalidStateException extends \Exception
{

    public function __construct(?string $state)
    {
        parent::__construct(sprintf('State [%s] is invalid', $state));
    }
}
