<?php

declare(strict_types=1);

namespace Grifix\StateMachine;

use Grifix\StateMachine\Exceptions\InvalidStateException;
use Grifix\StateMachine\Exceptions\TransitionIsNotPossibleException;

final class StateMachine
{

    private array $states = [];

    /** @var Transition */
    private array $transitions = [];

    public function __construct(Transition ...$transitions)
    {
        $this->transitions = $transitions;
        $this->extractStates();
    }

    public static function create(Transition ...$transitions): self
    {
        return new self(...$transitions);
    }

    private function extractStates(): void
    {
        foreach ($this->transitions as $transition) {
            $this->states[] = $transition->fromState;
            $this->states   = array_merge($this->states, $transition->toStates);
        }
        $this->states = array_values(array_unique($this->states));
    }

    public function getStates(): array
    {
        return $this->states;
    }

    public function isTransitionPossible(?string $fromState, ?string $toState): bool
    {
        $this->assertStateIsValid($fromState);
        $this->assertStateIsValid($toState);

        foreach ($this->transitions as $transition) {
            if ($transition->fromState !== $fromState) {
                continue;
            }
            if ( ! in_array($toState, $transition->toStates)) {
                continue;
            }

            return true;
        }

        return false;
    }

    /**
     * @throws TransitionIsNotPossibleException
     */
    public function assertTransitionIsPossible(?string $fromState, ?string $toState): void
    {
        if ( ! $this->isTransitionPossible($fromState, $toState)) {
            throw new TransitionIsNotPossibleException($fromState, $toState);
        }
    }

    private function assertStateIsValid(?string $state): void
    {
        if ( ! in_array($state, $this->states)) {
            throw new InvalidStateException($state);
        }
    }
}
