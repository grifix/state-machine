<?php
declare(strict_types=1);

namespace Grifix\StateMachine;

final class Transition
{
    public readonly array $toStates;

    public function __construct(public readonly ?string $fromState, ?string ...$toStates)
    {
        $this->toStates = $toStates;
    }
}
