<?php

declare(strict_types=1);

namespace Grifix\StateMachine\Tests;

use Grifix\StateMachine\Exceptions\InvalidStateException;
use Grifix\StateMachine\Exceptions\TransitionIsNotPossibleException;
use Grifix\StateMachine\StateMachine;
use Grifix\StateMachine\Transition;
use PHPUnit\Framework\TestCase;

final class StateMachineTest extends TestCase
{
    private readonly StateMachine $stateMachine;

    protected function setUp(): void
    {
        parent::setUp();
        $this->stateMachine = StateMachine::create(
            new Transition(null, 'new'),
            new Transition('new', 'active'),
            new Transition('active', 'closed', 'new')
        );
    }

    /**
     * @dataProvider transitionsDataProvider
     */
    public function testItChecksIfTransitionIsPossible(?string $fromState, ?string $toState, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, $this->stateMachine->isTransitionPossible($fromState, $toState));
    }

    public function transitionsDataProvider(): array
    {
        return [
            [null, 'new', true],
            ['new', 'active', true],
            ['active', 'closed', true],
            ['active', 'new', true],
            ['new', 'closed', false],
            ['closed', 'new', false],
            ['closed', 'active', false],
        ];
    }

    /**
     * @dataProvider invalidStatesDataProvider
     */
    public function testItDoesntAcceptInvalidState(
        string $fromState,
        string $toState,
        string $expectedInvalidState
    ): void {
        $this->expectException(InvalidStateException::class);
        $this->expectExceptionMessage(sprintf('State [%s] is invalid', $expectedInvalidState));
        $this->stateMachine->isTransitionPossible($fromState, $toState);
    }

    public function invalidStatesDataProvider(): array
    {
        return [
            ['old', 'new', 'old'],
            ['new', 'old', 'old']
        ];
    }

    /**
     * @dataProvider invalidTransitionsDataProvider
     */
    public function testItAssertsThatTransitionPossible(?string $fromState, ?string $toState): void
    {
        $this->expectException(TransitionIsNotPossibleException::class);
        $this->expectExceptionMessage(
            sprintf('Transition from state [%s] to state [%s] is not possible!', $fromState, $toState)
        );
        $this->stateMachine->assertTransitionIsPossible($fromState, $toState);
    }

    public function invalidTransitionsDataProvider(): array
    {
        return [
            [null, 'active'],
            [null, 'closed'],
            ['active', null],
            ['closed', null],
            ['new', 'closed'],
            ['closed', 'new'],
            ['closed', 'active'],
        ];
    }

    public function testItGetsStates(): void
    {
        self::assertEquals(
            [
                null,
                'new',
                'active',
                'closed'
            ],
            $this->stateMachine->getStates()
        );
    }
}
