Very simple state machine

# Installation

`composer require grifix/state-machine`

# Usage

```php
<?php

$stateMachine = new StateMachine(
    new Transition('new', 'active'),
    new Transition('active', 'closed', 'new')
);
$stateMachine->isTransitionPossible('new', 'active') //true
$stateMachine->isTransitionPossible('active', 'new') //true
$stateMachine->isTransitionPossible('new', 'closed') //false
$stateMachine->isTransitionPossible('closed', 'new') //false
$stateMachine->assertTransitionIsPossible('new', 'closed') //throws TransitionIsNotPossibleException
```
