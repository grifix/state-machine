<?php

declare(strict_types=1);

namespace Grifix\StateMachine\Exceptions;

final class TransitionIsNotPossibleException extends \Exception
{
    public function __construct(?string $fromState, ?string $toState)
    {
        parent::__construct(sprintf('Transition from state [%s] to state [%s] is not possible!', $fromState, $toState));
    }
}
